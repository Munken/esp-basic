#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>    
#include <ArduinoOTA.h>

#define LED_PIN 15

#include <PubSubClient.h>
const char* mqtt_server = "hassio.local";

void setup() {
  Serial.begin(115200);

  digitalWrite(LED_PIN, LOW);
  pinMode(LED_PIN, OUTPUT);
  Serial.println("Setup");

  WiFiManager wifiManager;
  wifiManager.autoConnect();

  Serial.println(WiFi.localIP());

  ArduinoOTA.begin();
}

bool mode = false;
int i = 0;
void loop() {
  ArduinoOTA.handle();

  digitalWrite(LED_PIN, mode);
  mode = !mode;
  Serial.println(i++);
  delay(1000);
}